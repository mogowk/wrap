// +build go1.12,wasm

// Package history allows manipulation of the browser session history,
// that is the pages visited in the tab or frame that the current page is loaded in
// Usage example:
//
//  history.ReplaceURL("/new/page/url")  // Set new url without new item in history
//  history.PushURL("/history/page/url") // Set new url with new item in history
//
// https://developer.mozilla.org/en-US/docs/Web/API/History
package history

import (
	"syscall/js"
)

// History object
type History struct {
	Value js.Value
}

// H is a History global value
var H *History

func init() {
	H = &History{Value: js.Global().Get("history")}
}

// PushState adds a state to the browser's session history stack.
// state - JavaScript object which is associated with the new history entry.
// title - Most browsers currently ignores this parameter, although they may use it in the future.
// url - The new history entry's URL is given by this parameter.
// Note that the browser won't attempt to load this URL after a call to PushState(),
// but it might attempt to load the URL later, for instance after the user restarts the browser.
//
// https://developer.mozilla.org/en-US/docs/Web/API/History/pushState
func PushState(state interface{}, title, url string) {
	H.Value.Call("pushState", state, title, url)
}

// ReplaceState method modifies the current history entry,
// replacing it with the state, title, and URL passed in the method parameters.
// This method is particularly useful when you want to update the state object
// or URL of the current history entry in response to some user action.
//
// https://developer.mozilla.org/en-US/docs/Web/API/History/replaceState
func ReplaceState(state interface{}, title, url string) {
	H.Value.Call("replaceState", state, title, url)
}

// PushURL is a wrap PushState just to set url and add it to history
func PushURL(url string) {
	PushState(nil, "", url)
}

// ReplaceURL is a wrap ReplaceState just to set new URL
// without adding it to history
func ReplaceURL(url string) {
	ReplaceState(nil, "", url)
}

// Back moves to backward through history.
// This acts exactly as if the user clicked on the Back button in their browser toolbar.
//
// https://developer.mozilla.org/en-US/docs/Web/API/History/back
func Back() {
	H.Value.Call("back")
}

// Forward moves forward as if the user clicked the Forward button.
//
// https://developer.mozilla.org/en-US/docs/Web/API/History/forward
func Forward() {
	H.Value.Call("forward")
}

// Go method loads a specific page from the session history.
// You can use it to move forwards and backwards through the history.
// -1 equivalent of calling Back()
//  1 just like calling Forward()
// -2 or 2 backwards by two pages or forward two pages
//  0 will reload the current page
//
// https://developer.mozilla.org/en-US/docs/Web/API/History/go
func Go(delta int) {
	H.Value.Call("go", delta)
}
