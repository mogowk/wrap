// +build go1.12,wasm

// Package cookie lets you read and write cookies associated with the document
// cookie package https://github.com/oskca/gopherjs-cookie thank you!
// Usage example:
//  println(cookie.IsCookieEnabled())
//  cookie.Set("test", "value", "/")
//  value, ok := cookie.Get("test")
//  println(ok)
//  println(value)
//
// https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie
package cookie

import (
	"strings"
	"syscall/js"
	"time"
)

// IsCookieEnabled returns false if setting a cookie will be ignored and true otherwise.
//
// https://developer.mozilla.org/en-US/docs/Web/API/Navigator/cookieEnabled
func IsCookieEnabled() bool {
	return js.Global().Get("navigator").Get("cookieEnabled").Bool()
}

func decodeURI(str string) string {
	return js.Global().Call("decodeURIComponent", str).String()
}

func encodeURI(str string) string {
	return js.Global().Call("encodeURIComponent", str).String()
}

// Get returns a given cookie by name. If the cookie is not set then ok will be set to false
func Get(name string) (value string, ok bool) {
	cookieStr := js.Global().Get("document").Get("cookie").String()
	if cookieStr == "" {
		return "", false
	}

	cookiePairs := strings.Split(cookieStr, "; ")
	for _, c := range cookiePairs {
		equalIndex := strings.IndexByte(c, '=')
		cookieName := c[:equalIndex]

		if cookieName == name {
			cookieValue := c[equalIndex+1:]
			return decodeURI(cookieValue), true
		}
	}

	return "", false
}

// SetString sets a cookie given a correctly formatted cookie string
// i.e "username=John Smith; expires=Thu, 18 Dec 2013 12:00:00 UTC; path=/"
func SetString(cookie string) {
	js.Global().Get("document").Set("cookie", cookie)
}

// Set adds a cookie to a user's browser with a name, value, expiry and
// path value, path and expires can be omitted
//
// Examples:
// Without path and expires: cookie.Set("name", "value", "")
//
// or with path and expires to next month:
// cookie.Set("name", "value", "/", time.Now().AddDate(0, 1, 0))
func Set(name, value, path string, expires ...time.Time) {
	if name == "" {
		return
	}

	var expiry string
	if len(expires) != 0 {
		e := expires[0]
		e = e.UTC()
		t := e.Format("Mon, 02 Jan 2006 15:04:05 UTC")
		expiry = "expires=" + t + "; "
	}

	if path != "" {
		path = " path=" + path
	}

	newCookie := name + "=" + encodeURI(value) + "; " + expiry + path
	SetString(newCookie)
}

// Delete deletes a cookie specified by name
func Delete(name string) {
	SetString(name + "=; expires=Thu, 01 Jan 1970 00:00:01 UTC;")
}
