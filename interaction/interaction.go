// +build go1.12,wasm

// Package interaction contains window interaction methods: alert, confirm, prompt
// Usage example:
//
//  ok := interaction.Confirm("confirm text")
//  if ok {
//    interaction.Alert("Thanks for your ok")
//  } else {
//    result := interaction.Prompt("Why did you click Cancel?", "")
//    interaction.Alert(result)
//  }
//
package interaction

import (
	"syscall/js"
)

// Alert displays an alert dialog with the message and an OK button
//
// https://developer.mozilla.org/en-US/docs/Web/API/Window/alert
func Alert(message string) {
	js.Global().Call("alert", message)
}

// Confirm displays a modal dialog with a message and two buttons: OK and Cancel.
// Returns a boolean indicating whether OK (true) or Cancel (false) was selected.
// If a browser is ignoring in-page dialogs, then result is always false.
//
// https://developer.mozilla.org/en-US/docs/Web/API/Window/confirm
func Confirm(message string) bool {
	return js.Global().Call("confirm", message).Bool()
}

// Prompt displays a dialog with an optional message prompting the user to input some text.
// Takes message to display to the user and default value displayed in the text input field.
// Returns text entered by the user or epmty string ""
//
// https://developer.mozilla.org/en-US/docs/Web/API/Window/prompt
func Prompt(message, defaultValue string) string {
	result := js.Global().Call("prompt", message, defaultValue)

	if result.IsNull() {
		return ""
	}

	return result.String()
}
