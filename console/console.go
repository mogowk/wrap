// +build go1.12,wasm

// Package console provides access to the browser's debugging console
// Usage example:
//
//  console.Time("console_log")
//  console.Log("some log")
//  console.TimeEnd("console_log") // Output: console_log: 4ms - timer ended
//
// https://developer.mozilla.org/en-US/docs/Web/API/Console
package console

import (
	"syscall/js"
)

// Console js object wrapper
type Console struct {
	Value js.Value
}

// C is a Console global value
var C *Console

func init() {
	C = &Console{Value: js.Global().Get("console")}
}

// Log outputs a message to the Web Console
// For general output of logging information
//
// https://developer.mozilla.org/en-US/docs/Web/API/Console/log
func Log(args ...interface{}) {
	C.Value.Call("log", args...)
}

// Debug outputs a message to the console with the log level "debug"
//
// https://developer.mozilla.org/en-US/docs/Web/API/Console/debug
func Debug(args ...interface{}) {
	C.Value.Call("debug", args...)
}

// Info is a informative logging of information
//
// https://developer.mozilla.org/en-US/docs/Web/API/Console/info
func Info(args ...interface{}) {
	C.Value.Call("info", args...)
}

// Warn outputs a warning message
//
// https://developer.mozilla.org/en-US/docs/Web/API/Console/warn
func Warn(args ...interface{}) {
	C.Value.Call("warn", args...)
}

// Error outputs an error message
//
// https://developer.mozilla.org/en-US/docs/Web/API/Console/error
func Error(args ...interface{}) {
	C.Value.Call("error", args...)
}

// Trace outputs a stack trace
//
// https://developer.mozilla.org/en-US/docs/Web/API/Console/trace
func Trace() {
	C.Value.Call("trace")
}

// Clear clears the console if the environment allows it
//
// https://developer.mozilla.org/en-US/docs/Web/API/Console/clear
func Clear() {
	C.Value.Call("clear")
}

// Time starts a timer you can use to track how long an operation takes.
// label identify the timer
//
// https://developer.mozilla.org/en-US/docs/Web/API/Console/time
func Time(label string) {
	C.Value.Call("time", label)
}

// TimeEnd stops a timer that was previously started by calling Time
//
// https://developer.mozilla.org/en-US/docs/Web/API/Console/timeEnd
func TimeEnd(label string) {
	C.Value.Call("timeEnd", label)
}

// TimeLog logs the current value of a timer that
// was previously started by calling Time
//
// https://developer.mozilla.org/en-US/docs/Web/API/Console/timeLog
func TimeLog(label string) {
	C.Value.Call("timeLog", label)
}

// Count logs the number of times that this particular call to Count() has been called
//
// https://developer.mozilla.org/en-US/docs/Web/API/Console/count
func Count(label string) {
	C.Value.Call("count", label)
}

// CountReset resets counter used with Count()
//
// https://developer.mozilla.org/en-US/docs/Web/API/Console/countReset
func CountReset(label string) {
	C.Value.Call("count", label)
}
