// +build go1.12,wasm

// Package location represents the location (URL) of the object it is linked to
// Usage example:
//  location.Reload(true)        // reload with ignoring the browser cache
//  location.Reload()            // reload like the Refresh button
//  println(location.Href())     // Output: http://localhost:8080/
//  println(location.FullPath()) // Output: /
//
// https://developer.mozilla.org/en-US/docs/Web/API/Location
package location

import (
	"syscall/js"
)

// Location object allows you to get any parts of the URL
// and set href fragments. If you want to set new URL use History object
type Location struct {
	Value js.Value
}

// L is a Location global value
var L *Location

func init() {
	L = &Location{Value: js.Global().Get("location")}
}

// Reload reloads the current resource, like the Refresh button.
// It has an optional Boolean parameter, which is false by default.
// If true, the page is always reloaded from the server, ignoring the browser cache
//
// https://developer.mozilla.org/en-US/docs/Web/API/Location/reload
func Reload(forcedReload ...bool) {
	if len(forcedReload) != 0 {
		L.Value.Call("reload", forcedReload[0])
	} else {
		L.Value.Call("reload")
	}
}

// Href returns string containing the entire URL
// Example: 'http://localhost:8080/' or 'http://localhost:8080/path/to/page?q=123#fragment'
//
// https://developer.mozilla.org/en-US/docs/Web/API/Location/href
func Href() string {
	return L.Value.Get("href").String()
}

// SetHref sets href and associated document navigates to the new page
//
// https://developer.mozilla.org/en-US/docs/Web/API/Location/href
func SetHref(href string) {
	L.Value.Set("href", href)
}

// Protocol returns string containing the protocol scheme of the URL including the final ':'.
// Example 'http:' or 'https:'
//
// https://developer.mozilla.org/en-US/docs/Web/API/Location/protocol
func Protocol() string {
	return L.Value.Get("protocol").String()
}

// Host returns string containing the host, that is the hostname, a ':', and the port of the URL.
// Example 'host.org' or 'localhost:8080'
//
// https://developer.mozilla.org/en-US/docs/Web/API/Location/host
func Host() string {
	return L.Value.Get("host").String()
}

// Hostname returns string containing the domain of the URL, without port
// Example: 'localhost' or 'host.org'
//
// https://developer.mozilla.org/en-US/docs/Web/API/Location/hostname
func Hostname() string {
	return L.Value.Get("hostname").String()
}

// Port returns string containing the port number of the URL
// Example: '8080' or '' if the URL does not contain an explicit port number
//
// https://developer.mozilla.org/en-US/docs/Web/API/Location/port
func Port() string {
	return L.Value.Get("port").String()
}

// Pathname returns string containing an initial '/' followed by the path of the URL
// Example: '/' or '/en-US/docs/Web/API/Location/pathname'
//
// https://developer.mozilla.org/en-US/docs/Web/API/Location/pathname
func Pathname() string {
	return L.Value.Get("pathname").String()
}

// Search returns string containing a '?' followed by the parameters or "querystring" of the URL
// Example: '' or '?q=123'
//
// https://developer.mozilla.org/en-US/docs/Web/API/Location/search
func Search() string {
	return L.Value.Get("search").String()
}

// Hash returns string containing a '#' followed by the
// fragment identifier of the URL.
// Example: '' or '#Examples'
//
// https://developer.mozilla.org/en-US/docs/Web/API/Location/hash
func Hash() string {
	return L.Value.Get("hash").String()
}

// Origin returns string containing the canonical form of the origin of the specific location
// Example: 'http://localhost:8080' or 'https://mozilla.org'
//
// https://developer.mozilla.org/en-US/docs/Web/API/Location/origin
func Origin() string {
	return L.Value.Get("origin").String()
}

// FullPath is just wrap to get current full path without host
// Example: 'path?query=param#fragment', in the root page the path will be '/'
func FullPath() string {
	return Pathname() + Search() + Hash()
}
