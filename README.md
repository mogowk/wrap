# wrap

Some wrappers for js callers

Usage example:

## console

Provides access to the browser's debugging console

```go
  console.Time("console_log")
  console.Log("Some log")
  console.TimeEnd("console_log") // Output: console_log: 4ms - timer ended
  console.Warn("Warn text")
```

## cookie

Lets you read and write cookies associated with the document

```go
  println(cookie.IsCookieEnabled())

  cookie.Set("test", "value", "/")

  value, ok := cookie.Get("test")
  println(ok)    // true
  println(value) // 'value'
```

## history

Allows manipulation of the browser session history

```go
  history.ReplaceURL("/new/page/url")  // Set new url without new item in history
  history.PushURL("/history/page/url") // Set new url with new item in history
```

## interaction

Contains window interaction methods: alert, confirm, prompt

```go
  ok := interaction.Confirm("confirm text")
  if ok {
    interaction.Alert("Thanks for your ok")
  } else {
    result := interaction.Prompt("Why did you click Cancel?", "")
    interaction.Alert(result)
  }
```

## location

Represents the location (URL) of the object it is linked to

```go
  location.Reload(true)        // reload with ignoring the browser cache
  location.Reload()            // reload like the Refresh button

  println(location.Href())     // Output: http://localhost:8080/
  println(location.FullPath()) // Output: /
```

## valueof

Panic safety wrapper of syscall/js.ValueOf

```go
  value, fatal := valueof.ValueOf(5)
  console.Log(fatal) // false
  console.Log(value) // 5

  value, fatal = valueof.ValueOf(struct{}{})
  console.Log(fatal) // true
  console.Log(value) // undefined
```
