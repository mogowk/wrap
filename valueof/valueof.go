// +build go1.12,wasm

// Package valueof is a panic safety wrapper of syscall/js.ValueOf
// Usage example:
//  value, fatal := valueof.ValueOf(5)
//  console.Log(fatal) // false
//  console.Log(value) // 5
//
//  value, fatal = valueof.ValueOf(struct{}{})
//  console.Log(fatal) // true
//  console.Log(value) // undefined
package valueof

import (
	"syscall/js"
)

// ValueOf returns x as a JavaScript value like a js.ValueOf but without panic
// returns fatal = true if js.ValueOf make panic when x is not one of the expected types.
func ValueOf(x interface{}) (value js.Value, fatal bool) {
	defer func() {
		if err := recover(); err != nil {
			fatal = true
		}
	}()

	value = js.ValueOf(x)
	return
}
